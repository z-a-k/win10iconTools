﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("win10iconTools")]
[assembly: AssemblyDescription("Windows 10 icon tools by Z.4k")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("z.4k")]
[assembly: AssemblyProduct("win10iconTools")]
[assembly: AssemblyCopyright("Copyright © z.4k 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fa38781b-eca5-4f30-9c37-6a5a70ff0936")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("0.5.5.0")]
[assembly: AssemblyFileVersion("0.5.5.0")]
