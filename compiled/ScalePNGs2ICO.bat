@echo off
REM esasier to edit and run from here

SET output=y.ico
SET input=input.png
SET interpolation=nearest
SET smoothing=none
SET mode=-png

REM my experiment results
SET dims=16,20,24,28,30,31,32,40,42,47,48,56,60,63,84,256

REM experimental set:
REM SET dims=8.2048

REM windows recommended and applied to all dpis:
REM SET dims=16,20,24,25,30,32,36,38,40,45,48,50,60,62,64,72,75,80,96,100,128,150,160,188,192,225,256,300,320,384,512,640,768,960,1024,1152,1536

REM other set:
REM SET dims=128

@echo on

win10iconTools pngscale2ico %output% %input% %interpolation% %smoothing% %dims% %mode%

@echo off

IF %ERRORLEVEL% NEQ 0 (
	echo.
	echo There were errors: returned error code: %ERRORLEVEL%
	pause
)